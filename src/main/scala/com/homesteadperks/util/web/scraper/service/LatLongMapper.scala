package com.homesteadperks.util.web.scraper.service

import com.koddi.geocoder.{Location, Geocoder}

object LatLongMapper {
  val geo = Geocoder.create("AIzaSyA-OBtHB3GgDTglhe6mOP2IYoggXayCTJY")
  def apply(address: String): (Number, Number) = {
    val results = geo.lookup(address)
    val location = if(results.length > 0) results.head.geometry.location else Location(0, 0)
    (location.latitude, location.longitude)
  }
}
