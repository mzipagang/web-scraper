package com.homesteadperks.util.web.scraper
import akka.actor.{ActorRef, Props, ActorSystem}
import akka.stream.ActorMaterializer
import com.homesteadperks.util.web.scraper.model.Perk
import com.homesteadperks.util.web.scraper.repository.PerkRepository
import com.homesteadperks.util.web.scraper.service.{HtmlMapper, LatLongMapper}


object Main {
  implicit val system = ActorSystem("http-client")
  implicit val materializer = ActorMaterializer()

  def main(args: Array[String]) : Unit = {
    var perkRepo: ActorRef = null
    if(Config.mongoHost != null && Config.mongoDb != null) {
      perkRepo = system.actorOf(Props[PerkRepository], name = "perkRepo")
    }

    var (address: String, pages: Int) = (args(0), args(1).toInt)
    (for (i <- 0 to 10) yield s"https://www.yelp.com/search?find_loc=${address.replaceAll(" ", "+")}&start=${i * 10}&sortby=rating")
    .foreach { url => {
      println(s"Processing: $url")
      val perks = HtmlMapper.get(url).map { p =>
        address = if (p.Address.length > 10) p.Address else address
        val (latitude, longitude) = LatLongMapper(address)
        Perk(url, p.Name, p.Description, p.Tags, p.Image, address, p.Phone, latitude, longitude)
      }
      println(perks.size)
      if(Config.mongoHost != null && Config.mongoDb != null) {
        perkRepo ! perks
      }
    }}
  }
}