package com.homesteadperks.util.web.scraper.model

case class Perk (
                  Url: String,
                  Name: String,
                  Description: String,
                  Tags: String,
                  Image: String,
                  Address: String,
                  Phone: String,
                  Latitude: Number,
                  Longitude: Number
                )
