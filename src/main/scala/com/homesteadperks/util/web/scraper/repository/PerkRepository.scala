package com.homesteadperks.util.web.scraper.repository

import akka.actor.Actor
import com.homesteadperks.util.web.scraper.Config
import com.homesteadperks.util.web.scraper.model.Perk
import org.mongodb.scala._

class PerkRepository extends Actor {
  val mongoClient: MongoClient = MongoClient(Config.mongoHost)
  val database: MongoDatabase = mongoClient.getDatabase(Config.mongoDb)
  val collection: MongoCollection[Document] = database.getCollection("specialtys")

  def receive = {
    case list: List[Perk] => {
      val docs: List[Document] = list.map { p => Document(
        "name" -> p.Name,
        "description" -> p.Description,
        "tags" -> p.Tags,
        "geo_with_lat_lon" -> Document("lat" -> p.Latitude.toString, "lon" -> p.Longitude.toString),
        "data" -> Document("address" -> p.Address, "url" -> p.Url, "phone" -> p.Phone),
        "images" -> List(p.Image)
      )}

      collection.insertMany(docs).subscribe(new Observer[Completed] {
        def onNext(result: Completed): Unit = println(s"Inserted: ${list.head.Url}")

        def onError(e: Throwable): Unit = println(s"Failed: ${e.getMessage}")

        def onComplete(): Unit = {}
      })
    }
  }
}
