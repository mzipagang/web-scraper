package com.homesteadperks.util.web.scraper.service

import com.homesteadperks.util.web.scraper.model.Perk
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.model.Element


object HtmlMapper {
  val browser = JsoupBrowser()

  def get(uri: String): List[Perk] = {
    val doc = browser.get(uri)

    val items = doc >> elementList(".column-alpha li.regular-search-result")

    def accumulatePerks(elements: List[Element], perks: List[Perk]): List[Perk] = {
      elements match {
        case Nil => perks
        case p :: tail => accumulatePerks(tail, perks :+ elementToPerk(p))
      }
    }

    def elementToPerk(e: Element): Perk = {
      // title
      val title = e >> allText("h3.search-result-title span.indexed-biz-name a.biz-name span")
      val tags = (e >> elementList(".category-str-list a")).map(a => a.text).mkString(", ")
      val desc = (e >> allText(".biz-extra-info .snippet-block .snippet")).replace("read more", "")
      val addr = e >> allText(".secondary-attributes address")
      val hood = e >> allText(".secondary-attributes .neighborhood-str-list")
      val city = e >> allText(".secondary-attributes .biz-city")
      val phone = e >> allText(".secondary-attributes .biz-phone")
      val fullAddr = s"$addr ${if(city != "") city else hood}"
      // From the meta element with "viewport" as its attribute name, extract the
      // text in the content attribute
      val image = (e >> elementList("a[data-analytics-label=biz-photo] img")).head >> attr("src")

      Perk(uri, title, desc, tags, image, fullAddr, phone, 0, 0)
    }

    accumulatePerks(items, List())
  }
}
