package com.homesteadperks.util.web.scraper

import java.nio.charset.Charset

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object Config {
  implicit class RichConfig(val underlying: com.typesafe.config.Config) extends AnyVal {
    def getOptionalString(path: String): Option[String] = if (underlying.hasPath(path)) {
      Some(underlying.getString(path))
    } else {
      None
    }
  }

  java.security.Security.setProperty("networkaddress.cache.ttl" , "30")

  val UTF8 = Charset.forName("UTF-8")
  val config = ConfigFactory.load()

  val mongoHost = System.getenv("MONGO_HOST")

  val mongoDb = System.getenv("MONGO_DB")

  val actorSystem = ActorSystem("WebScraper")
}