import _root_.sbtassembly.AssemblyPlugin.autoImport._
import com.typesafe.sbt.SbtStartScript
import sbt.Keys._
import sbt._

organization := "com.homesteadperks.util"

name := "web-scraper"

version := "0.01"

scalaVersion := "2.12.1"

publishTo := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/.m2/repository")))

resolvers += "Local Maven Repository" at "file:///"+Path.userHome.absolutePath+"/.m2/repository"

libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.5.0"

libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.1"

libraryDependencies += "com.typesafe" % "config" % "1.3.0"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.12"

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.12"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.1"

libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.12"

libraryDependencies += "com.koddi" %% "geocoder" % "1.1.0"

libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "2.1.0"

libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.4.1"

Revolver.settings

seq(SbtStartScript.startScriptForClassesSettings: _*)

assemblyMergeStrategy in assembly :=  {
  // This next line breaks grpc
  //case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x if x.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
  case x if x == "reference.conf" => MergeStrategy.concat
  case _ => MergeStrategy.first
}
