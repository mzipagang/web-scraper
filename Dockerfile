FROM openjdk:8
RUN mkdir -p /opt/homesteadperks/web-scraper
COPY target/scala-*/*-assembly-*.jar /opt/homesteadperks/web-scraper/web-scraper-assembly.jar
