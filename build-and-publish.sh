#!/bin/bash
sbt clean assembly
COMMIT_HASH=`git rev-parse HEAD`
echo "commit: $COMMIT_HASH"
docker build --no-cache -t homesteadperks/web-scraper:$COMMIT_HASH .

